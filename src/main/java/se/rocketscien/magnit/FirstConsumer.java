package se.rocketscien.magnit;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.util.function.Function;

public class FirstConsumer implements AutoCloseable {

    private static final Logger log = LoggerFactory.getLogger(FirstConsumer.class);

    // Create a Connection
    private final Connection connection;

    // Create a Session
    private final Session session;

    // Create a MessageConsumer from the Session to the Topic or Queue
    private final MessageConsumer consumer;

    public FirstConsumer(String brokerUrl, int sessionType, Function<Session, Destination> destinationExtractor)
            throws JMSException {
        // Create a ConnectionFactory
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);

        connection = connectionFactory.createConnection();
        connection.start();

        connection.setExceptionListener(e ->
                log.error("JMS Exception occurred. Shutting down client.", e)
        );

        session = connection.createSession(sessionType == Session.SESSION_TRANSACTED, sessionType);

        // Create the destination (Topic or Queue)
        Destination destination = destinationExtractor.apply(session);

        consumer = session.createConsumer(destination);
    }

    @Override
    public void close() throws JMSException {
        consumer.close();
        session.close();
        connection.close();
    }

    public Message consumeMessage(int timeout) throws JMSException {
        // Wait for a message
        Message message = consumer.receive(timeout);
        if (message == null) return null;

        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            String text = textMessage.getText();
            log.info("Received text: " + text);
        } else {
            log.info("Received message: " + message);
        }

        message.acknowledge();
        if (session.getTransacted()) {
            session.commit();
        }

        return message;
    }

    public Message consumeMessageNoWait() throws JMSException {
        // Wait for a message
        Message message = consumer.receiveNoWait();
        if (message == null) return null;

        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            String text = textMessage.getText();
            log.info("Received text: " + text);
        } else {
            log.info("Received message: " + message);
        }

        message.acknowledge();

        return message;
    }

}
