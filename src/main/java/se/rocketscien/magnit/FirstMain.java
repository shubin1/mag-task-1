package se.rocketscien.magnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.Arrays;

public class FirstMain {

    private static final Logger log = LoggerFactory.getLogger(FirstMain.class);

    private static final String brokerUrl = "tcp://localhost:61616";
    private static final String embeddedBrokerUrl = "vm://localhost";

    public static void main(String[] args) throws JMSException {
        first();
    }

    public static void first() throws JMSException {
        String currentBrokerUrl = brokerUrl;

        for (Integer sessionType : Arrays.asList(
                Session.SESSION_TRANSACTED,
                Session.AUTO_ACKNOWLEDGE,
                Session.CLIENT_ACKNOWLEDGE,
                Session.DUPS_OK_ACKNOWLEDGE
        )) {
            log.info("SessionType: " + sessionType);
            Instant start = Instant.now();

            FirstConsumer firstQueueConsumer = new FirstConsumer(
                    currentBrokerUrl,
                    sessionType,
                    session -> FirstMain.createQueue(session, "TEST-Q")
            );

            FirstConsumer firstTopicConsumer = new FirstConsumer(
                    currentBrokerUrl,
                    sessionType,
                    session -> FirstMain.createTopic(session, "TEST-T")
            );

            FirstProducer firstQueueProducer = new FirstProducer(
                    currentBrokerUrl,
                    sessionType,
                    DeliveryMode.NON_PERSISTENT,
                    session -> FirstMain.createQueue(session, "TEST-Q")
            );

            FirstProducer firstTopicProducer = new FirstProducer(
                    currentBrokerUrl,
                    sessionType,
                    DeliveryMode.NON_PERSISTENT,
                    session -> FirstMain.createTopic(session, "TEST-T")
            );


            OffsetDateTime now = OffsetDateTime.now();
            firstQueueProducer.sendMessage("Queue message at " + now);
            firstTopicProducer.sendMessage("Topic message at " + now);

            // Flash all messages
            while (firstQueueConsumer.consumeMessage(1000) != null) { }
            while (firstTopicConsumer.consumeMessage(1000) != null) { }

            Instant end = Instant.now();

            log.info((end.toEpochMilli() - start.toEpochMilli()) + " milliseconds");

            firstQueueProducer.close();
            firstTopicProducer.close();

            firstQueueConsumer.close();
            firstTopicConsumer.close();
        }
    }

    protected static Destination createTopic(Session session, String topicName) {
        try {
            return session.createTopic(topicName);
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    protected static Destination createQueue(Session session, String queueName) {
        try {
            return session.createQueue(queueName);
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

}