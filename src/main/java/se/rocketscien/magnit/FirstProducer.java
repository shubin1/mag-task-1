package se.rocketscien.magnit;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.util.function.Function;

public class FirstProducer implements AutoCloseable {

    private static final Logger log = LoggerFactory.getLogger(FirstProducer.class);

    // Create a Connection
    private final Connection connection;

    // Create a Session
    private final Session session;

    // Create a MessageConsumer from the Session to the Topic or Queue
    private final MessageProducer producer;

    public FirstProducer(
            String brokerURL,
            int sessionType,
            int deliveryMode,
            Function<Session, Destination> destinationExtractor
    ) throws JMSException {
        // Create a ConnectionFactory
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerURL);

        // Create a Connection
        connection = connectionFactory.createConnection();
        connection.start();

        // Create a Session
        session = connection.createSession(sessionType == Session.SESSION_TRANSACTED, sessionType);

        // Create the destination (Topic or Queue)
        Destination destination = destinationExtractor.apply(session);

        // Create a MessageProducer from the Session to the Topic or Queue
        producer = session.createProducer(destination);
        producer.setDeliveryMode(deliveryMode);
    }

    public void sendMessage(String text) throws JMSException {
        TextMessage message = session.createTextMessage(text);

        // Tell the producer to send the message
        log.info("Sent message: " + message.hashCode() + " : " + Thread.currentThread().getName());
        producer.send(message);

        if (session.getTransacted()) {
            session.commit();
        }
    }

    @Override
    public void close() throws JMSException {
        session.close();
        connection.close();
    }
}
